#Installation

Tout d'abord, clonez ce référentiel:
$ git clone https://gitlab.com/Kayme98/esgi-projet
Ensuite, placez votre application Symfony dans un symfonydossier et n'oubliez pas de l'ajouter symfony.localhostdans votre /etc/hostsfichier.
Ensuite, lancez:
$ docker-compose up
Vous avez terminé, vous pouvez visiter votre application Symfony à l’URL suivante: http://symfony.localhost(et accéder à Kibana sur http://symfony.localhost:81)
Remarque: vous pouvez reconstruire toutes les images Docker en exécutant:
$ docker-compose build

#Comment ça fonctionne?

Voici les docker-composeimages construites:


php: Ceci est le conteneur PHP-FPM comprenant le volume d'application monté sur,

nginx: Ceci est le conteneur du serveur Web Nginx dans lequel les volumes php sont également montés,

Cela se traduit par les conteneurs en cours d'exécution suivants:
> $ docker-compose ps
        Name                       Command               State              Ports
--------------------------------------------------------------------------------------------
dockersymfony_nginx_1   nginx                            Up      443/tcp, 0.0.0.0:80->80/tcp
dockersymfony_php_1     php-fpm7 -F                      Up      0.0.0.0:9000->9000/tcp
<<<<<<< HEAD
```

#Lire les journaux
Vous pouvez accéder aux journaux des applications Nginx et Symfony dans les répertoires suivants de votre ordinateur hôte:

logs/nginx
logs/symfony
