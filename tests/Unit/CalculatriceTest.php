<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Service\Calculatrice;

class StarWarsApiTest extends TestCase
{
    public function testAddidtion()
    {
        $Calculatrice = new Calculatrice();
        $this->assertEquals(40, $Calculatrice->addition(20, 20));
    }
 
    public function testMultiplication()
    {
        $Calculatrice = new Calculatrice();
        $this->assertEquals(100,  $Calculatrice->multiplication(10, 10));
    }

    public function testDivision()
    {
        $Calculatrice = new Calculatrice();
        $this->assertEquals(5, $Calculatrice->division(20, 4));
    }
	public function testSoustraction()
    {
        $Calculatrice = new Calculatrice();
        $this->assertEquals(10, $Calculatrice->soustraction(20, 10));
    }
}
