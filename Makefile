install:
	composer install


test_unit:
	bin/phpunit tests/Unit

test: test_unit 

start:
	php bin/console server:run
