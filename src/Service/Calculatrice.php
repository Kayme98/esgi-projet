<?php

namespace App\Service;

class Calculatrice {
    public function addition($n1, $n2) {
        return ($n1 + $n2);
    }

    public function soustraction($n1, $n2) {
        return ($n1 - $n2);
    }

    public function multiplication($n1, $n2) {
        return ($n1 * $n2);
    }

    public function division($n1, $n2) {
        return ($n1 / $n2);
    }
}

